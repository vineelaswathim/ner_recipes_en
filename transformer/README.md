# ner_baseline: BERT with Medical NER, first baseline model

References: 
- [Transformers-Token-Classification](https://github.com/huggingface/transformers/tree/master/examples/pytorch/token-classification)
- [TorchServe - Hugglingface Examples](https://github.com/pytorch/serve/blob/master/examples/Huggingface_Transformers/Transformer_handler_generalized.py)

# Datasets

- BC5CDR - CHEMICAL and DISEASE entities with test, train and dev files

- data - test and train files are same as BC5CDR but the predict file is a sample data containing patient's discharge and has diseases from ICD code

# Models


- `/media/volume/aicner/exp/ner/vs_19` - BERT model with patient's discharge data

- `/media/volume/aicner/exp/ner/vs_med1` - ElectraMED (Benchmark model) with patient's discharge data

