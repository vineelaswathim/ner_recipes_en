import torch
from transformers import (
  BertTokenizerFast,
  AutoModelForTokenClassification,
  AutoTokenizer
)

id2label = {
    "0": "B-Chemical",
    "1": "B-Disease",
    "2": "I-Chemical",
    "3": "I-Disease",
    "4": "O"
}

def inference_vs(model_dir, input_text, max_length=150, device="cpu"):
    input_size = len(input_text)
    model = AutoModelForTokenClassification.from_pretrained(model_dir)
    model.to(device)
    model.eval()

    tokenizer = AutoTokenizer.from_pretrained(
        model_dir, do_lower_case=True
    )

    input_ids_batch = None
    attention_mask_batch = None
    inputs = tokenizer.encode_plus(
        input_text, 
        max_length=int(max_length), 
        pad_to_max_length=True, 
        add_special_tokens=True, 
        return_tensors='pt'
    )
    input_ids = inputs["input_ids"].to(device)
    attention_mask = inputs["attention_mask"].to(device)

    if input_ids.shape is not None:
        if input_ids_batch is None:
            input_ids_batch = input_ids
            attention_mask_batch = attention_mask
        else:
            input_ids_batch = torch.cat((input_ids_batch, input_ids), 0)
            attention_mask_batch = torch.cat((attention_mask_batch, attention_mask), 0)

    inferences = []
    outputs = model(input_ids_batch, attention_mask_batch)[0]
    num_rows = outputs.shape[0]

    for i in range(num_rows):
        output = outputs[i].unsqueeze(0)
        predictions = torch.argmax(output, dim=2)
        tokens = tokenizer.tokenize(tokenizer.decode(input_ids_batch[i]))
        prediction = [(token, id2label[str(prediction)]) for token, prediction in zip(tokens, predictions[0].tolist())]
        inferences.append(prediction)

    results = inferences[0][:input_size]
    return results

def extract_labels(results):
    """ Extract labels from the results sequence.
    Ex:
        [{start: 0, end: 8, text: "將感染的部位切除", labels: ["treatment"]}]
    """
    label_list = []
    length = len(results)

    tmp_label = None
    for idx, result in enumerate(results):
        token, full_label = result

        if "B-" in full_label:
            label = full_label.replace("B-", "")
            i_label = "I-"+label
            tmp_label = {"start": idx, "end": idx+1, "text": token, "labels": [label]}

            if (idx == length - 1) or (results[idx+1][1] != i_label):
                label_list.append(tmp_label)
                tmp_label = None
        
        if "I-" in full_label:
            tmp_label["end"]+=1
            tmp_label["text"]+=token
            if (idx == length - 1) or (results[idx+1][1] != full_label):
                label_list.append(tmp_label)
        # tmp_label = {start: idx, end: idx+1, text: result[0], labels: [label]}
    return label_list

if __name__ == "__main__":
    input_text = " After birth, poor activity, respiratory distress and grunting were noted. Throughout the course, there was no trauma history, travel history, nor cluster history. Under the impression of premature with respiratory distress, he was admitted for further evaluation and management. "
    # input_text = "姆哈哈太郎"
    results = inference_vs(
        model_dir="./media/volume/aicner/exp/ner/vs_19",
        input_text=input_text
    )
   # for result in results:
    #    print(result)
    list1 = [input_text]
 
    print(extract_labels(results))

    # [
    #     {'start': 12, 'end': 13, 'text': 'x', 'labels': ['CHECK']}, 
    #     {'start': 20, 'end': 24, 'text': '降糖藥物', 'labels': ['TREATMENT']}, tmp/media/volume/aicner/exp/ner/vs_symp
    #     {'start': 25, 'end': 27, 'text': '下肢', 'labels': ['BODY']}
    # ]

