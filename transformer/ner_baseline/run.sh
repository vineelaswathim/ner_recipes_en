#!/bin/bash
set -euo pipefail

###################################
#                                 #
# VS: fine-tuned from BERT #
#                                 #
###################################

# export CUDA_VISIBLE_DEVICES="0"
export CUDA_VISIBLE_DEVICES=""

python3 train_ner_distill-Copy2.py \
  --model_name_or_path "bert-base-cased" \
  --tokenizer_name "bert-base-cased" \
  --dataset_name "dataset_csv" \
  --output_dir "./media/volume/aicner/exp/ner/vs_19" \
  --do_train \
  --do_eval \
  --do_predict
